#! /usr/bin/env python
#
# Copyright (C) 2007, Michiel van Baak
# Michiel van Baak <michiel@vanbaak.info>
#
# This file is part of PureUserAdmin.
#
# PureUserAdmin is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PureUserAdmin is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import md5
import cherrypy
import mako.template, mako.lookup
import sqlalchemy as sa

__version__ = "0.1.0"

# template handling
def Template(tmpl_file, vars):
	global tmpl_lookup

	vars['version'] = __version__
	cherrypy.request.app.logged_in = cherrypy.session.get('logged_in')
	vars['logged_in'] = cherrypy.session.get('logged_in')
	vars['root_dir'] = root_dir

	t = tmpl_lookup.get_template(tmpl_file+'.html')
	return(t.render(**vars))

class user(object):

	@cherrypy.expose
	def index(self):
		return 'user object'

	@cherrypy.expose
	def edit(self, username=None):
		if not cherrypy.session.get('logged_in'):
			return Template('login', {'title':'login', 'from_page':root_dir+'/user/edit'})
		if username == None:
			#create a new user
			user = DBUSERS()
		else:
			#edit a specific user
			user = session.query(DBUSERS).get_by(username=username)
		return Template('edit', {'title':'edit', 'user':user})

	@cherrypy.expose
	def save(self, username=None, password=None, uid='1003', gid='1003', homedir=None, quota=None, new=False):
		if not cherrypy.session.get('logged_in'):
			return Template('login', {'title':'login', 'from_page':root_dir+'/user/list'})
		if username != None and password != None and homedir != None:
			if new == "True":
				user = DBUSERS()
			else:
				user = session.query(DBUSERS).get_by(username=username)

			if password != user.password:
				user.password = md5.new(password).hexdigest()

			user.username = username
			user.uid      = uid
			user.gid      = gid
			user.dir      = homedir

			session.save(user)
			session.flush()
			session.close()
			cherrypy.lib.cptools.redirect(root_dir+'/user/list', False)
		else:
			return 'ERROR, some fields were empty'

	@cherrypy.expose
	def delete(self, username=None):
		if not cherrypy.session.get('logged_in'):
			return Template('login', {'title':'login', 'from_page':root_dir+'/user/list'})
		if username != None:
			user = session.query(DBUSERS).get_by(username=username)
			session.delete(user)
			session.flush()
			session.close()
			cherrypy.lib.cptools.redirect(root_dir+'/user/list', False)
		else:
			return 'Error, No username given'
	
	@cherrypy.expose
	def list(self, username=None):
		if not cherrypy.session.get('logged_in'):
			return Template('login', {'title':'login', 'from_page':root_dir+'/user/list'})
		if username == None:
			users = session.query(DBUSERS).select()
		else:
			users = session.query(DBUSERS).get_by(username=username)
		return Template('list', {'title':'Userlist', 'users':users})

class pua(object):
	user = user()

	@cherrypy.expose
	def index(self):
		return Template('main', {'title':'main', 'data':'Welcome to PureUserAdmin'})

	@cherrypy.expose
	def about(self):
		return Template('main', {'title':'about'})

	@cherrypy.expose
	def show_login(self, from_page=None):
		if from_page == None:
			from_page = root_dir

		return Template('login', {'title':'login', 'from_page':from_page})

	@cherrypy.expose
	def login(self, username, password, from_page=None):
		if from_page == None:
			from_page = root_dir

		if (username == cherrypy.config['pua.username']) and (password == cherrypy.config['pua.password']):
			cherrypy.session['logged_in'] = True
		else:
			cherrypy.session['logged_in'] = False

		cherrypy.lib.cptools.redirect(from_page, False)

	@cherrypy.expose
	def logout(self):
		cherrypy.session.pop('logged_in')
		cherrypy.lib.cptools.redirect(root_dir+'/', False)

#
# Set up cherrypy so it's independent of the path being run from, and load the
# configuration.
#
path_base = os.path.dirname(__file__)
path_config = os.path.join(path_base, 'pua.ini')
path_templates = os.path.join(path_base, 'templates')

cherrypy.config.update(path_config)

# find out if we have pua.root_dir set
if 'pua.root_dir' in cherrypy.config:
	root_dir = cherrypy.config['pua.root_dir']
else:
	root_dir = ''

#
# Set up template lookup
#
tmpl_lookup = mako.lookup.TemplateLookup(
	directories=[path_templates], 
	output_encoding='utf-8', 
	encoding_errors='replace',
	default_filters=['decode.utf8']
)

#
# Setup the database
#
metadata = sa.BoundMetaData(cherrypy.config['pua.database'])
tbl_logins = sa.Table('logins', metadata,
	sa.Column('username', sa.String(160), primary_key=True),
	sa.Column('password', sa.String(255)),
	sa.Column('uid', sa.Integer),
	sa.Column('gid', sa.Integer),
	sa.Column('dir', sa.String(255)),
)
metadata.create_all()
class DBUSERS(object):
	pass;
sa.mapper(DBUSERS, tbl_logins)

#
# Setup sqlalchemy session
#
session = sa.create_session()

#
# These methods take care of running the application via either mod_python or
# stand-alone using the built-in CherryPy server.
#
def start_modpython():
	"""
	Method used by mod_python.
	Builtin cherrypy http server is not used.
	"""
	cherrypy.tree.mount(pua(), root_dir, config=path_config)
	cherrypy.engine.SIGHUP = None
	cherrypy.engine.SIGTERM = None
	try:
		cherrypy.engine.start(blocking=False)
	except:
		cherrypy.log(traceback=True)
		raise

def start_standalone():
	"""
	Method with builtin http server
	Used when ran from the commandline
	This is not intended for production use
	"""
	cherrypy.quickstart(pua(), config=path_config)

#
# If we're not being imported, it means we should be running stand-alone.
#
if __name__ == '__main__':
	start_standalone()
